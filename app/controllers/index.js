/*
 * Some general notes
 * 
 * The purpose of this architecture is so you have to deal with as little platform specific
 * logic as possible. It should allow you to focus on getting the UI done. You shouldn't 
 * have to worry about how to open a new window on different platforms, or replicating 
 * common code throughout each controller (handling Android actionbar specific code). 
 * It's all taken care of in the background. 
 * 
 * Just focus on the implementation of the screen you're supposed to work on.
 */

var WindowHelper = require("WindowHelper");
var TabHelper = require("TabHelper");

var tabGroup = TabHelper.createTabGroup();

function init()
{	
	/*
	 * The following only applies to the base window inside of a tab.
	 * 
	 * If the window you want should be inside a tabGroup, pass a reference of the tabGroup.
	 * Pass a reference of the window inside the tab.
	 * Then set the current tab of the window.
	 * 
	 * Just do what the next 4 lines of code are doing if its a new tab:
	 */
	
	var win1 = WindowHelper.createWindow({ui:"sample/form", tabGroup:tabGroup});
	var tab1 = TabHelper.createTab({win:win1, title:"Tab 1"});
	win1.setTab(tab1);
	tabGroup.addTab(tab1);
	
	var win2 = WindowHelper.createWindow({ui:"sample/flightDetails", tabGroup:tabGroup});
	var tab2 = TabHelper.createTab({win:win2, title:"Tab 2"});
	win2.setTab(tab2);
	tabGroup.addTab(tab2);
}

init();
tabGroup.open();