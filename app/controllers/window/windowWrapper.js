/*
 * Majority of the logic is encapsulated in here.
 * Takes care of updating share text on Android, takes care of opening
 * windows on different platforms and handles how to close windows.
 * 
 * You shouldn't have to touch anything in here!!!
 */

var WindowHelper = require('WindowHelper');
var TabHelper = require("TabHelper");

var args = arguments[0] || {};
var UI = null;
var tab = null;
var tabGroup = null;

var topPosition = 0;
var bottomPosition = 0;
var stillNeedsPositionSetting = true;

if (OS_ANDROID) 
{
	var ABX = require("ActionBarExtra");
	
	var openAnimation = {
		activityEnterAnimation : Ti.App.Android.R.anim.slideinright, 
		activityExitAnimation  : Ti.App.Android.R.anim.scaleout
	};
	
	var closeAnimation = {
		activityEnterAnimation : Ti.App.Android.R.anim.scalein, 
		activityExitAnimation  : Ti.App.Android.R.anim.slideoutright
	};
}


function init()
{
	var options = {share:false};
	
	if (args.ui)
	{
		UI = args.ui;
		setUI(UI);
		
		if (UI.onWinInit)
		{
			var options = UI.onWinInit();
			for (key in options) 
			{
				var value = options[key];
				$.win[key] = value;
				
				Logger.print("setting for key: " + key + " with value: " + value);
			}
		}
		
		if (UI.showABXShare)
		{
			Logger.print("UI.showABXShare is true");
			
			options.share = true;
		}
		
		if (OS_IOS)
		{
			if (UI.getLeftNavWinButtons)
			{
				var buttons = UI.getLeftNavWinButtons();
				$.win.leftNavButtons = buttons;
			}
			
			if (UI.getRightNavWinButtons)
			{
				var buttons = UI.getRightNavWinButtons();
				$.win.rightNavButtons = buttons;
			}
		}
	}
	
	if (OS_ANDROID)
	{
		tabGroup = args.tabGroup;
		
		if (Ti.Platform.Android.API_LEVEL >= 21)
		{
			bottomPosition = 48;
			
			if (tabGroup)
			{
				topPosition = 121;
			}
			else
			{
				topPosition = 72;
			}
		}
		
		ABX.setup(options);
		
		
		if (UI)
		{
			if (UI.getView() instanceof Ti.UI.ScrollView)
			{
				Logger.print("its a scrollview! adding some extra padding to the bottom for translucency");
				
				UI.getView().add(Ti.UI.createView({
					bottom:0,
					width:40,
					height:bottomPosition,
					backgroundColor:"red"
				})); 
				
				$.container.top = topPosition;
				stillNeedsPositionSetting = false;
			}
		}
	}
	
	if (stillNeedsPositionSetting)
	{	
		$.container.top = topPosition;
		$.container.bottom = bottomPosition;
	}
}

function setUI(ui)
{
	$.container.add(ui.getView());
}

function onWinOpen(e)
{
	//Logger.print("onWinOpen");
	
	if (OS_ANDROID)
    {
    	// if this window is NOT part of a tab group
    	if (!tabGroup)
    	{
    		var actionBar = $.win.activity.actionBar;
    		if (actionBar)
    		{
    			actionBar.onHomeIconItemSelected = onABXHomeIconPressed;
    			actionBar.displayHomeAsUp = true;
    		}
    		else
    		{
    			Logger.error("Actionbar is not available");
    		}
    		
	    	Logger.print("$.win.getActivity(): " + $.win.getActivity());
	    	
	    	exports.updateABXShareText();
    	}
    }
	
	if (UI)
	{
		if (UI.onWinOpen)
		{
			UI.onWinOpen(e);
		}
	}
}

function onWinClose(e) 
{
	//Logger.print("onWinClose");
	
	if (UI)
	{
		if (UI.onWinClose)
		{
			UI.onWinClose(e);
		}
	}
}

function onWinFocus(e)
{
	//Logger.print("onWinFocus");
	
	if (UI)
	{
		if (UI.onWinFocus)
		{
			UI.onWinFocus(e);
		}
		
		// if the window is inside a tabGroup, update the actionbar stuff
		if (OS_ANDROID)
		{
			if (tabGroup)
			{	
				Logger.print("tabGroup is valid");
				
				var params = {};
				if (UI.getABXInitParams)
				{
					params = UI.getABXInitParams();
				}
				
				params.onABXHomeIconPressed = onABXHomeIconPressed;
				
				params.share = UI.showABXShare || false;
				params.tabGroup = tabGroup;
				
				TabHelper.updateShareText(params);
			}
		}
	}
}

function onABXHomeIconPressed()
{
	Logger.print("onABXHomeIconPressed");
	
	if (UI)
	{
		if (UI.onABXHomeIconPressed)
		{
			UI.onABXHomeIconPressed();
		}
	}
	
	// don't auto close window if it's inside a tabGroup
	if (!tabGroup)
	{
		exports.closeWindow();
	}
}

exports.updateABXShareText = function()
{
	if (OS_ANDROID)
	{
		if (UI)
		{
			if (UI.getABXInitParams)
			{
				var params = UI.getABXInitParams();
				
				var activity = $.win.getActivity();
				if (tabGroup)
				{
					activity = tabGroup.getActivity();
				}
				
				params.activity = activity;
				
				ABX.init(params);
			}
		}
	}
};

exports.setTab = function(_tab)
{
	Logger.print("setTab");
	
	tab = _tab;
};


exports.openWindow = function(args)
{
	Logger.print("openWindow");
	
	if (tab)
	{
		var win = WindowHelper.createWindow(args);
		win.setTab(tab);
		
		if (OS_ANDROID)
		{
			win.getView().open(openAnimation);
		}
		else 
		{
			tab.open(win.getView());
		}
	}
	else
	{
		Logger.error("tab not set, not opening window");
	}
};

exports.closeWindow = function()
{
	Logger.print("closeWindow");
	
	if (OS_ANDROID) 
	{
		$.win.close(closeAnimation);
	}
	else
	{
		if (tab)
		{
			tab.close($.win);
		}
		else
		{
			$.win.close();
		}
	}
};


init();