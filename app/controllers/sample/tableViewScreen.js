var args = arguments[0] || {};

function onRelease(e)
{
	Alloy.Collections.user.fetch({
		success:e.hide,
		error:e.hide
	});
}

function init()
{
	// required for the inital fetch, must be done via the pullToRefresh widget
	$.pullToRefresh.refresh();
}

init();
