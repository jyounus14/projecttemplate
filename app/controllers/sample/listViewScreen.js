// this UI controller shows an example of how to use a listview with pull to refresh using this module: https://github.com/fokkezb/nl.fokkezb.pullToRefresh

var args = arguments[0] || {};
var win = null;

exports.setWin = function(parent)
{
	win = parent;
};

function onRelease(e) 
{
    // hide the refresher after 3 seconds
    setTimeout(function()
    {
    	e.hide();
    }, 3000);
}

function onItemPressed(e)
{
	if (win)
	{
		win.openWindow({ui:"sample/tableViewScreen"});
	}
}
