var args = arguments[0] || {};
var win = null;

exports.showABXShare = true;
exports.getABXInitParams = function()
{
	return {
		title:"App name here", // ActionBar title
		subtitle:"Flight Details", // ActionBar subtitle
		shareText:"Share text for tab 2" // ActionBar share text 
	};
};

exports.onWinFocus = function(e)
{
	Logger.print("onWinFocus flightDetails UI");
};

exports.setWin = function(parent)
{
	win = parent;
};

exports.onABXHomeIconPressed = function()
{
	$.container.backgroundColor = "yellow";
};


function buttonPressed(e)
{
	Logger.print("opening new win from flightDetails");
	if (win)
	{
		win.openWindow({ui:"sample/flightDetailsConfirmed"});
	}
}
