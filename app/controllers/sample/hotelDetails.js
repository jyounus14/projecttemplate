var args = arguments[0] || {};
var shareText = "Some text here";
var win = null;

exports.showABXShare = true;
exports.getABXInitParams = function()
{
	return {
		title:"Title here", // ActionBar title
		subtitle:"Hotel Details", // ActionBar subtitle
		shareText:shareText // ActionBar share text 
	};
};

exports.setWin = function(parent)
{
	win = parent;
};

function emailPressed()
{
    var emailDialog = Ti.UI.createEmailDialog();

    emailDialog.subject = "hotel_email_subject";
    emailDialog.messageBody = "hotel_email_body";
    emailDialog.toRecipients = ["test@test.com"];

    emailDialog.open();
}

function callPressed()
{
    //Ti.Platform.openURL("tel:01234567890");
    if (win)
    {
    	win.openWindow({ui:"sample/listViewScreen"});
    }
}

function mapPressed()
{	
    alert("map pressed\n updating share text");
    shareText = "share text should have updated";
    
    if (win)
    {
    	win.updateABXShareText();
    }
}
