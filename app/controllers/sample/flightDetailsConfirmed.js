var NavWinButtonHelper = require("NavWinButtonHelper");

var args = arguments[0] || {};
var win = null;
var navWinButton = null;

exports.getABXInitParams = function()
{
	return {
		title:"Intrepid", // ActionBar title
		subtitle:"Flight Details" // ActionBar subtitle
	};
};

exports.setWin = function(parent)
{
	win = parent;
};

exports.getRightNavWinButtons = function()
{
	if (!navWinButton)
	{
		navWinButton = NavWinButtonHelper.createShareButton({callback:navWinButtonPressed});
	}
	
	return [navWinButton];
};

function navWinButtonPressed(e)
{
	alert("share button pressed");
}

function buttonPressed(e)
{
	Logger.print("opening new win from hotelDetails");
	if (win)
	{
		win.openWindow({ui:"sample/hotelDetails"});
	}
}
