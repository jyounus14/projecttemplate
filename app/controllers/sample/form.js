/*
 * Note:
 * 
 * Checkout "sample/hotelDetails.js" to see how to update the shareText on Android.
 * You can also see an example for opening a new window there as well. 
 */

var NavWinButtonHelper = require("NavWinButtonHelper");

var args = arguments[0] || {};
var leftNavButton = null;
var rightNavButton = null;

var win = null;

/*
 * Define this if you need a reference to the current window object.
 * If you want to open/close a window or update the Android actionbar share text, you want to do this. See note above.
 */
exports.setWin = function(parent)
{
	win = parent;
};


/*
 * iOS specific.
 * Return the left navigation window button here if you want to set one (or multiple ones).
 */
exports.getLeftNavWinButtons = function()
{
	if (!leftNavButton)
	{
		leftNavButton = NavWinButtonHelper.createShareButton({callback:leftNavWinButtonPressed});
		leftNavButton.backgroundColor = "blue";
	}
	
	return [leftNavButton];
};

/*
 * iOS specific.
 * Return the right navigation window button here if you want to set one (or multiple ones).
 */
exports.getRightNavWinButtons = function()
{
	if (!rightNavButton)
	{
		rightNavButton = NavWinButtonHelper.createShareButton({callback:rightNavWinButtonPressed});
	}
	
	return [rightNavButton];
};


/*
 * Android specific.
 * Set true if you want share options to show in the top right hand corner of the actionbar.
 * If set to true, you MUST provide "shareText" property inside the getABXInitParams function!
 */
exports.showABXShare = true;

/*
 * Android specific.
 * Define the actionbar title, subtitle and sharetext.
 */
exports.getABXInitParams = function()
{
	return {
		title:"App name here",
		subtitle:"Random Form",
		shareText:"My awesome share text!"
	};
};

/*
 * Android specific.
 * Event called when the actionbar home icon is pressed.
 */
exports.onABXHomeIconPressed = function()
{
	Logger.print("onABXHomeIconPressed");
};


/*
 * Event called when window is opened.
 */
exports.onWinOpen = function(e)
{
	Logger.print("onWinOpen form UI");
};

/*
 * Event called when window is about to be closed.
 */
exports.onWinClose = function(e)
{
	Logger.print("onWinClose form UI");
};

/*
 * Event called when window receives focus.
 */
exports.onWinFocus = function(e)
{
	Logger.print("onWinFocus form UI");
};

/*
 * Event called when window is about to be initialised. 
 * Set any custom options here that you want to apply to the Ti.UI.Window object.
 */
exports.onWinInit = function()
{
	Logger.print("onWinInit form UI");
	
	var options = {
		title:"Register"
	};
	
	if (OS_ANDROID)
	{
		options.windowSoftInputMode = Ti.UI.Android.SOFT_INPUT_STATE_HIDDEN;
	}
	
	return options;
};


/*
 * The callback you set when creating the left navigation window button.
 * You can rename this to whatever you pass into the callback parameter for NavWinButtonHelper.
 * See getLeftNavWinButtons function.
 */
function leftNavWinButtonPressed(e)
{
	alert("left nav win button pressed");
}

/*
 * The callback you set when creating the right navigation window button.
 * You can rename this to whatever you pass into the callback parameter for NavWinButtonHelper.
 * See getRightNavWinButtons function.
 */
function rightNavWinButtonPressed(e)
{
	alert("right nav win button pressed");
}