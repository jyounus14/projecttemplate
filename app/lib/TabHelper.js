var ABX = require("ActionBarExtra");

function createTab(args)
{
	var win = args.win.getView();
	var title = args.title || "";
	var icon = args.icon || "";
	
	if (!win.title)
	{
		win.title = title;
	}
	
	var tab = Ti.UI.createTab({
		window:win,
		title:title,
		icon:icon
	});
	
	return tab;
}


function createTabGroup(args)
{
	var tabGroup = Ti.UI.createTabGroup();
	tabGroup.addEventListener('open', onTabGroupOpen);
	
	return tabGroup;
}

function onTabGroupOpen(e)
{
	Logger.print("tabgroup opened: " + e.source);
	
	var tabGroup = e.source;
	updateShareText({tabGroup:tabGroup});
}

function updateShareText(args)
{
	if (OS_ANDROID)
	{
		Logger.print("updateShareText args: " + args);
		
		var tabGroup = args.tabGroup;
		var actionBar = tabGroup.activity.actionBar;
		
		var share = args.share || false;
		var shareText = args.shareText || "";
		var title = args.title || "";
		var subtitle = args.subtitle || "";
		
		ABX.setup({share:share});
		
		if (actionBar)
		{
			actionBar.onHomeIconItemSelected = args.onABXHomeIconPressed;
			actionBar.displayHomeAsUp = false;
			
			var params = {activity:tabGroup.getActivity(),
						  shareText:shareText,
						  title:title,
						  subtitle:subtitle};
			
			ABX.init(params);
		}
		else
		{
			Logger.error("Actionbar is not available");
		}
	}
}

exports.createTab = createTab;
exports.createTabGroup = createTabGroup;
exports.updateShareText = updateShareText;