/******************************************************************************
* Copyright         : 2014 Ophir Oren (WEBe)
* File Name         : log4ti.js
* Description       : The main loggin engine.
*                    
* Revision History  :
* Date				Author    		Comments
* ---------------------------------------------------------------------------
* 06/11/2014		Ophir Oren		Created.
*
/*****************************************************************************/

var logger;
var _needRebuild = true;
var _appenders = [];
var _appendersMapping = [];
var _loggingLevels = ['trace', 'debug', 'info', 'warn', 'error', 'fatal', 'stackTrace'];

exports.configuration = {
	includeLineNumbers: false,
	
	addLevel: function(level) {
		_loggingLevels.push(level);
		_needRebuild = true;
	},
	
	loadAppender: function(name, appender, configuration) {
		// validate appender name
		if (!name) throw 'bad appender name';
		if (_appenders[name]) throw 'appender name already in use';
		
		// validate appender
		if (!appender) throw 'no appender was supplied';
		
		// initiate appender
		var obj = require(appender);
		switch (typeof obj) {
			case 'object':
				_appenders[name] = appender;
				break;
			case 'function':
				_appenders[name] = new (require(appender))();
				break;
			default:
				throw 'unknown appender object';
		}
			
		obj.configure(configuration);
	},
	
	mapAppender: function(name, levels) {
		// validate appender name
		if (!name) throw 'bad appender name';
		if (!_appenders[name]) throw 'appender "' + name + '" was not found. you must load appenders before mapping them.';
		
		for (var i = 0; i < levels.length; i++) {
			var levelName = levels[i];
			if (!_appendersMapping[levelName])
				_appendersMapping[levelName] = [];
			
			_appendersMapping[levelName].push(name);
			
			levelName = null;
		}
		
		_needRebuild = true;
	}
};

exports.createLogger = function() {
	if (!logger || _needRebuild) {
		buildLogger();
	}
	
	return logger;
};

function buildLogger() {
	logger = {};
	
	function functionMaker(level) {
		return function(m) {
			var appenderMap = _appendersMapping[level];
			if (!appenderMap) return;
			
			if (exports.configuration.includeLineNumbers && level != 'stackTrace') {
				var lineNumber = getLineNumber();
				if (lineNumber) m += ' ' + lineNumber;
			}
			
			if (level == 'stackTrace') {
				var stacktrace = getStacktrace();
				if (stacktrace) m += '\n' + stacktrace;
			}
			
			for (var i = 0; i < appenderMap.length; i++) {
				var appender = _appenders[appenderMap[i]];
				if (typeof appender === 'string')
					appender = require(appender);
				
				
				appender.write(level, m);
			}
		};
	};
	
	for (var i = 0; i < _loggingLevels.length; i++) {
		var level = _loggingLevels[i];
		logger[level] = functionMaker(level);
	}
	
	_needRebuild = false;
};

function getStacktrace() {
	var error = new Error();
	if (error.stack) return error.stack;
	return null;
};

function getLineNumber() {
	var stacktrace = getStacktrace();
	if (!stacktrace) return null;
	
	var errorLines = stacktrace.split("\n");
	//Ti.API.info("errorLines: " + errorLines);
	
	// iOS simulator
	
	// getStacktrace@file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/log4ti/log4ti.js:30:26,
	// getLineNumber@file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/log4ti/log4ti.js:36:35,
	// file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/log4ti/log4ti.js:7:47,
	// print@file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/Logger.js:9:41,
	// Controller@file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/alloy/controllers/index.js:50:17,
	// createController@file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/alloy.js:264:54,
	// global code@file:///Users/jayy/Library/Developer/CoreSimulator/Devices/7496D925-6D6F-44E6-8BA4-26FA3EAD9D58/data/Containers/Bundle/Application/BEF6DCA2-E01F-4D60-9078-D041824C82D2/ProjectTemplate.app/app.js:5:23
	
	// Android
	
	// Error,    
	// at getStacktrace (log4ti/log4ti.js:30:17),    
	// at getLineNumber (log4ti/log4ti.js:36:22),    
	// at Object.info (log4ti/log4ti.js:7:34),    
	// at Object.print (Logger.js:11:37),    
	// at Window.onWinOpen (alloy/controllers/window/windowWrapper.js:21:16),    
	// at Window.<anonymous> (ti:/events.js:49:21),    
	// at Window.<anonymous> (ti:/events.js:101:19)
	
	var index = OS_IOS ? 4 : 5;
	var errorIndex = Math.min(index, errorLines.length-1); //Math.min(3, errorLines.length-1);
	var errorLine = errorLines[errorIndex];
	
	var returnString = "[";
	returnString += extractFunctionName(errorLine) + "@";
	returnString += extractFileNameAndLine(errorLine) + "]";
	
	return returnString;
};

function extractFunctionName(errorLine) 
{
	if (OS_IOS)
	{
		return errorLine.split("@")[0];
	}
	
	var line = errorLine.trim();
	var lines = line.split(" ");
	
	return lines[1];
}

function extractFileNameAndLine(errorLine) 
{
	var path = null;
	
	if (OS_IOS)
	{
		path = errorLine.split("/");
	}
	else 
	{
		var line = errorLine.trim();
		var lines = line.split(" ");
		var fileName = lines[2];	
		fileName = fileName.substr(1, fileName.length - 2);
		
		path = fileName.split("/");
	}
		
	
	var startIndex = -1;
	for (var i = 0; i < path.length; i++)
	{
		var text = path[i];
		if (text == "alloy")
		{
			startIndex = ++i;
			break;
		}
	}
	
	var fileName = "";
	for (var i = startIndex; i < path.length; i++)
	{
		var text = path[i];
		fileName += "/" + text;
	}
	
	return fileName;
}
