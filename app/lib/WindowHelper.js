function createWindow(args)
{
	var uiController = args.ui;
	var tabGroup = args.tabGroup || null;
	
	var ui = Alloy.createController(uiController);
	var win = Alloy.createController("window/windowWrapper", {ui:ui, tabGroup:tabGroup});
	
	if (ui.setWin)
	{
		ui.setWin(win);
	}
	
	return win;
}

exports.createWindow = createWindow;
