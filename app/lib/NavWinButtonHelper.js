function createShareButton(args)
{
	var button = Ti.UI.createView({
		width:33, 
		height:33, 
		backgroundColor:"red"
	});
	
	if (args.callback)
	{
		button.addEventListener("click", args.callback);
	}
	
	return button;
}

exports.createShareButton = createShareButton;