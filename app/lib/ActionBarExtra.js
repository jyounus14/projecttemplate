/*jshint -W030 */

/**
* Lib: actionBarExtras wraps the ActionBarExtras module by Ricardo Alcocer of Appcelerator
* [https://github.com/ricardoalcocer/actionbarextras/](https://github.com/ricardoalcocer/actionbarextras/)
* @class lib.actionBarExtras
*/

var abx, share, opts;

/**
* @method setup
* com.alcoapps.actionbarextras needs to be required and opts passed in BEFORE window is opened due to way Android Activity works on Android - http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.Android.Activity
* @return null
*
*     @example
*     // Run Setup BEFORE window opens
*     var abx = require('actionBarExtras');
*     var opts = {
*         share: true
*     };
*     abx.setup(opts);
*/
function setup(args) {
    if(Ti.Platform.Android.API_LEVEL >= 11) {
        // override share var so we can use it in later function
        share = args.share;
        abx = require('com.alcoapps.actionbarextras'),
          opts = {
                shareAction: share,
                titleFont: false,
                subtitleFont: false
            };
    }
}
exports.setup = setup;

/**
* @method init
* Initialises window options when 'open' event fires. This is most useful for configuring Android ActionBarExtras
* @return null
*
*     @example
*     var params = {
*        activity: $.win.getActivity(), // Current Window Activity to attach ActionBar to. Must get activity AFTER window opened otherwise it is empty
*        title: 'TUI India', // ActionBar Title
*        subtitle: 'Register', /// ActionBar SubTitle
*        shareText: 'Sharing my TUI Holiday' // Text to share. Only used if opts.share set to true in init
*     };
*     abx.init(params);
*/
function init(args) {
    if(Ti.Platform.Android.API_LEVEL >= 11) {
        if(args.activity){
            args.activity.invalidateOptionsMenu(); // Declares that the option menu has changed and should be recreated.
            args.activity.onCreateOptionsMenu = function(e){

                if (share){
                    // This is how you add a basic Share Action to your ActionBar
                    // this should be done within the onCreateOptionsMenu
                    // because we need to pass a reference to the menu

                    // at first, create a basic share intent
                    var intent = Ti.Android.createIntent({
                        action: Ti.Android.ACTION_SEND,
                        type: 'text/plain'
                    });
                    intent.putExtra(Ti.Android.EXTRA_TEXT, args.shareText);

                    // now pass over the menu and the intent like this
                    abx.addShareAction({
                        menu: e.menu,
                        intent: intent,
                        showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS // optional - default: SHOW_AS_ACTION_IF_ROOM
                        //title: "Sharing my TUI India Holiday" // optional - default: "Share"
                    });
                }
            };
        }

        abx.title = args.title; // actionbar title
        abx.subtitle = args.subtitle; //actionbar subtitle
       	//abx.setBackgroundColor('#A1C8E5FF'); // actionbar background color
        //abx.setTitleColor('#3893B4'); //actionbar title text color
        //abx.setSubtitleColor('#333'); //actionbar subtitle text color
    }


    // Force Portrait orientation on Android on this activity
    args.activity.requestedOrientation = Titanium.Android.SCREEN_ORIENTATION_PORTRAIT;
}
exports.init = init;
