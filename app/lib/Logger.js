var log4ti = require('log4ti/log4ti');
var shouldPrint = true;

var logger = null;

function init()
{
	// log4ti configuration
	// this should be configured only once
	log4ti.configuration.includeLineNumbers = true; // include line numbers (when possible)
		
	//log4ti.configuration.addLevel('myCustomLevel'); // adding a custom logging level
	
	// load appenders
	log4ti.configuration.loadAppender('consoleAppender', 'log4ti/log4ti_appenders/console_appender');
	log4ti.configuration.loadAppender('fileAppender', 'log4ti/log4ti_appenders/file_appender');
	
	// attach appenders to levels
	log4ti.configuration.mapAppender('consoleAppender', ['trace', 'debug', 'info', 'warn', 'error', 'fatal', 'stackTrace']);
	log4ti.configuration.mapAppender('fileAppender', ['warn', 'error', 'fatal']);
	
	
	logger = log4ti.createLogger();
}

function print(message)
{
	if (shouldPrint)
	{
		if (logger)
		{
			logger.info(message);
		}
	}
}

function error(message)
{
	if (shouldPrint)
	{
		if (logger)
		{
			logger.error(message);
		}
	}
}


init();

exports.print = print;
exports.error = error;
