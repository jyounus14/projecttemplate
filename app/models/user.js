exports.definition = {
	config: {
		columns: {
            id:"INTEGER PRIMARY KEY",
            name:"text",
            username:"text",
            email:"text",
            address:"blob",
            phone:"text",
            website:"text",
            company:"blob"
        },
        URL:"http://jsonplaceholder.typicode.com/users",
        debug:1, //debug mode enabled
        useStrictValidation:0, // validates each item if all columns are present
        adapter: {
            type: "sqlrest",
            collection_name: "users",
            idAttribute: "id"
        },
        deleteAllOnFetch: true
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};